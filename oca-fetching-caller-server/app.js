const path = require('path');
const mongoose = require('mongoose');
const dotenv=require('dotenv');
const Agenda=require('agenda');
dotenv.config({
    path: './_configs/.env.config'
});
global.appRoot = path.resolve(__dirname);

let redisQ=require('redis');
var clientRedis=redisQ.createClient(6379,process.env.REDIS_SERVER);

clientRedis.on('connect', function() {
    console.log('connected with redis server');
});

global.redisClient=clientRedis;

clientRedis.setex("ahmad",100,"Hai");
clientRedis.get("ahmad",function(err,result){
    console.log(result)
})

let loggers = require('./_configs/logger');
global.logger = loggers;
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);
mongoose.connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    poolSize: 10
});
mongoose.Promise = global.Promise;
mongoose.connection.on('connected',()=>{
    console.log("connected to mongodb ")
})
mongoose.connection.on('error', (err) => {
    console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));
    process.exit();
});


//requiring master mode
require('./_models/contact-group');
//require('./_models/call-broadcast');


const {MongoClient}=require("mongodb");

async function run(){
    const mongoConnectionString=process.env.MONGODB_URI1;
    try{
        const db = await MongoClient.connect(mongoConnectionString,{ useNewUrlParser: true,autoReconnect:true,poolSize: 15});
        const agd =new Agenda().mongo(db.db(),'jobs');
        return new Promise((resolve,reject)=>{
          agd.once('ready',resolve(agd));
        })
    }catch(err){
        loggers.info("CANNOT MAKING CONNECTION WITH SCHEDULER");
        console.log(err);
    }
}
var agenda;
(async function(){
    agenda=await run();
    require('./jobs/call-fetching')(agenda);

    agenda.defaultConcurrency(10);
    agenda.defaultLockLimit(10);
   //agenda.now(process.env.JOB_NAME,{"status":"Starting","additional":{"_id":"5d45336bdd963e00177a6de3","uid":"464d3d6f-22b2-49a5-a273-ccf393ab3708","presence_id":"464d3d6f-22b2-49a5-a273-ccf393ab3708","campaign_name":"tes","application":"5354b096-2f08-4de6-a5c1-fbefbe3261c4","account_code":"464d3d6f-22b2-49a5-a273-ccf393ab3708","phonebook":"5cd0f18b6adcac1991f42cf7","call_setting":"5c4daab2c67e3725c4a8a4b2","call_immediately":"true","call_status":"Preparation","user_id":"5c8f1e56f9ce350accdfb9e0","created_at":"2019-08-03T07:10:35.000Z","updated_at":"2019-08-03T07:10:35.000Z","contact_count":1,"call_estimated_time":"0 hour(s) and 0 minute(s).","__v":0},"setting":{"weekdays":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"_id":"5c4daab2c67e3725c4a8a4b2","caller_id_name":"Telkom","setting_name":"Call Setting ZONA WIB","caller_id_number":"+62280600777","dialing_timeout":90,"max_call_duration":1000,"concurent_limit":200,"schedule_minute_per":1,"retries_call":0,"retries_call_between":180,"daily_start_time":"06:30","status":"active","daily_end_time":"21:00","type":"all","user_id":"","timezone":"Asia/Jakarta","gateway_id":"c7b66330-5c4c-4bcd-8ec7-b399af4dd9a8","updated_id":"","updated_at":"2019-01-27T12:56:54.000Z","created_at":"2019-01-27T12:56:54.000Z","__v":0},"nextRow":0})
    agenda.start();
})();

// (async function(){
//     try{

//         let nextRow=job.attr.additionalData

//         console.log(process.env.DEFAULT_CONCURRENT_LIMIT_PER_USERS);
//         //get call setting by 
//         let callSetting=await setting.getCallSetting("5c4daab2c67e3725c4a8a4b2");
//        // -->get contact by concurent limit -> update subscribers to in_queue -> send to message queueing
//        // --> in queue message update call subscribers to in_call if proccesing and requeue if not
//        //check contact subscribers to define concurrent limit
//        //status in_queue
//        let sumContactSubscribers=await subcriber.countSubscribers("5c5b7fdde97b76011d8113cd","in_queue");
//         if(sumContactSubscribers==0){
//             //get 100 concurent limit
//             let contacts=await contactList.getContactList({contact_group:"5d42802aea5d0d00297bd255"},process.env.DEFAULT_CONCURRENT_LIMIT_PER_USERS);
//             let parameter={};
//             parameter['contact_group']=additionalData.phonebook;
//             if(nextRow!=0){
//                 parameter['_id']={'$gt':ObjectId(last_id)};
//             }


//             let last=contacts[contacts.length].Od
//             //insert subcriberCollection
//         }

//        //get data contact by concurent lmit
//     }catch(err){

//     }
    
// })();

async function graceful() {
    console.log("the job is down, remove main jobs");
    await agenda.stop();
    mongoose.connection.close(false,()=>{
      console.log('MongoDb connection closed.');
      process.exit(0);
    })
    process.exit(0);
}
process.on('SIGTERM', graceful);
process.on('SIGINT' , graceful);