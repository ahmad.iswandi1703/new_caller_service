const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const moment=require('moment-timezone');
const timezone=require('../_configs/config.json').SERVER_DEFAULT_TIMEZONE;



let BroadcastCall= new Schema({
    uid:String,
    presence_id:String,
    campaign_name:String,
    account_code:String,
    application:String,
    phonebook:{
        type:Schema.Types.ObjectId,
        ref:'BroadcastGroup',
        require:true
    },
    call_setting:{
        type:Schema.Types.ObjectId,
        ref:'CallSetting',
        require:true
    },
    contact_count:Number,
    call_immediately:String,
    call_schedule:Date,
    call_status:String,
    call_estimated_time:String,
    user_id:String,
    created_at:Date,
    updated_at:Date
    //add property to add advanced call_scheduled

});

let vCall=mongoose.model('BroadcastCall',BroadcastCall);

exports.updateCallStatus=async function(broadcast,status){
    return new Promise(async function(resolve,reject){
        vCall.findById(broadcast,function(err,res){
            const DateNow=moment().tz(timezone);
            res.updated_at=DateNow.format();
            res.call_status=status;
            res.save(function(err){
                if (err) {
                    console.log("error");
                };
                resolve("success");
            })
        })
    })
}

exports.callBroadcast=mongoose.model('BroadcastCall',BroadcastCall);