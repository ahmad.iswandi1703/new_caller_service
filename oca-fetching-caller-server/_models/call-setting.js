const mongoose=require('mongoose');
const Schema=mongoose.Schema;


let CallSetting= new Schema({
    caller_id_name:String,
    caller_id_number:String,
    dialing_timeout:Number,
    max_call_duration:Number,
    concurent_limit:Number,//default 10
    retries_call:Number,
    retries_call_between:Number,//in minutes;
    daily_start_time:String,
    daily_end_time:String,
    schedule_minute_per:Number,
    status:String,
    weekdays:[String],
    type:String, /// all user (default) specific user if all user 
    user_id:String,
    updated_id:String,
    created_at:Date,
    updated_at:Date,
    timezone:String,
    gateway_id:String, //get from v_gateway from postgredb,
    setting_name:String
});

let vCallSetting=mongoose.model('CallSetting',CallSetting);

//get call setting

exports.getCallSetting=async function(id){
    return new Promise((resolve,reject)=>{
        try{
            redisClient.get(`getCallSettingByID:${id}`,async (err,result)=>{

                if(result){
                    const results=JSON.parse(result);
                    resolve(results);
                }else{         
                    let resultData=await vCallSetting.findById(id);
                    if(resultData){
                       redisClient.setex(`getCallSettingByID:${id}`,parseInt(process.env.REDIS_EXPIRED_DATA),JSON.stringify(resultData));
                        resolve(resultData);
                    }else{
                        reject("cannot found the data");
                    }
                }
            });
        }catch(err){
            reject(err);
        }
    })
}