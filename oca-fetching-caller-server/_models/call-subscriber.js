const mongoose=require('mongoose');
const Schema=mongoose.Schema;

const ObjectId=mongoose.Types.ObjectId

let BroadcastSubscriberSchema= new Schema({
    phone:String,
    phone_id:String,
    created_at:Date,
    updated_at:Date,
    uuid:String,
    broadcast:{
        type:Schema.Types.ObjectId,
        ref:'BroadcastCall',
        require:true
    },
    call_setting:String,
    status:String, //(in_queue, in_call, no_answer, answer,re_call, error, fail, finish)
    tryCall:Number,
    data:Map
});


//
let vSubscriber=mongoose.model('BroadcastSubscriber',BroadcastSubscriberSchema);
//get broadcast subscribers
exports.countSubscribers=async function(broadcast,status){
    return new Promise((resolve,reject)=>{
        vSubscriber.countDocuments({broadcast:ObjectId(broadcast),status:status}).then(function(res){
            resolve(res);
        }).catch(function(err){
            reject(err);
        })
    })
}

exports.insertBulk=async function(collection){
    return new Promise((resolve,reject)=>{
        vSubscriber.insertMany(collection,function(error,docs){
            if(error){
                reject(err);
            }
            resolve(docs);
        })
    });
    
}
//insert broadcast_subscribers

exports.insertCollection=async function(dta){
    return new Promise((resolve,reject)=>{
        let sub=new vSubscriber(dta);
    
        sub.save((err)=>{
           
            if(err){
                reject(err);
            }
            resolve(sub);
        })
    })
}