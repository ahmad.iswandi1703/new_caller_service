var amqp = require('amqplib/callback_api');

/**
 * 
 * @param {} payload 
 * this  message queue will broadcast to all consumer for sms exchange 
 * it is send to message gateway and callback processing
 */
module.exports=function(payload){

    return new Promise((resolve,reject)=>{
        amqp.connect(process.env.AMQP, function (error0, connection) {
            if (error0) {
                logger.info(`cannot connect to queueu ${error0}`);
                reject(error0);
            }
            connection.createChannel(async function (error1, channel) {
                if (error1) {
                    throw error1;
                }
                var msg = JSON.stringify(payload);
      
                let queue="queue_call_existing";
                channel.assertQueue(queue, {
                    durable: true
                });
                try{
                    channel.sendToQueue(queue, Buffer.from(msg), {
                        persistent: true
                    });


                    resolve();
                }catch(Err){
                   
                    reject(Err);
                }
            });

            setTimeout(()=>{
                connection.close();
            },5000);


        });
    })

}
