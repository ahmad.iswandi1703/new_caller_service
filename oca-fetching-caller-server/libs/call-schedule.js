const moment=require('moment-timezone');

let changeHoursOfDateNow=function(dateNow,time){
    var startDay=time;//get from setting
    var split=startDay.split(":");
    var newDate=dateNow.set({
        hours:split[0],
        minutes:split[1]
    }).format();

    return newDate;
}
let settingModule={
    settingNextrun:function(setting){

        let nDate = moment().tz(setting.timezone); 
        let dateNow=moment().tz(setting.timezone);
        let numOfDday=dateNow.day();//sunday=0, monday=1,......, saturday=6
        let nextRun=new Date(Date.now() + 2000);
        let weekDays=setting.weekdays;
        
        var timeNow=moment().tz(setting.timezone).format();
        var timeEnded=changeHoursOfDateNow(dateNow,setting.daily_end_time);
        //alocated to another day
        if(timeNow>=timeEnded){
            //stop the job and set nextRun At to tommorrow at start the day
            let nextRunAt="";
            //it mean weeken or saturday
            let daysBetween=0;
            if(numOfDday == 6){
                //it means saturday  add to the next day
                let foundNextDay=false;
                let indexDay=0; 
                //console.log(weekDays[0].length);
                while(!foundNextDay){
                    if(weekDays[indexDay].length!=0 && weekDays[indexDay]!=null){
                        //check if next day is the day that forbided in weekdays
                        nextRunAt= weekDays[indexDay];
                        foundNextDay=true;
                    }
                    indexDay++;
                }
                daysBetween=indexDay;
                //if there is no next day then stop the broadcast
            }else{
                let foundNextDay=false;
                let indexDay=(numOfDday)+1; 
                let sumDay=0;
                
                //console.log(weekDays[0].length);
                while(!foundNextDay){
                    if(weekDays[indexDay].length!=0 && weekDays[indexDay]!=null){
                        nextRunAt= weekDays[indexDay];
                        foundNextDay=true;
                    }
                    if(indexDay==weekDays.length-1){
                        indexDay=0;
                    }else{
                        indexDay++;
                    }
                    sumDay=sumDay+1;
                }
                daysBetween=sumDay;
                //if there is no next day then stop the broadcast
            }

            var dateNext=dateNow.add(daysBetween,'days').tz(setting.timezone);
            var startDay=setting.daily_start_time;//get from setting
            var split=startDay.split(":");
            var newDate=dateNext.set({
                hours:split[0],
                minutes:split[1]
            }).format();
            nextRun=newDate;
        }else{
            //lewat 1 menit, Allocated to the next schedule minute
            var nextSchedule=setting.schedule_minute_per;
            ///change next schedule with seconds manner , for fast proccessing wkwkw
            nextRun=moment(timeNow).add(20,'seconds').tz(setting.timezone).format();
        }

        return nextRun;
    }
}

module.exports=settingModule;