

const moment=require('moment-timezone');

let vSetting=require('../_models/call-setting');
let contactList=require('../_models/contact-list');
let subcriber=require('../_models/call-subscriber');
let broadcast=require('../_models/call-broadcast');
const scheduleSetting=require('../libs/call-schedule');
const mongoose=require('mongoose');
const ObjectId=mongoose.Types.ObjectId;
const  SERVER_DEFAULT_TIMEZONE="Asia/Jakarta";

const queue=require('../libs/message-queue');
module.exports=function(scheduler){
  
    scheduler.define(process.env.JOB_NAME,async function(job,done){

        let id=job.attrs._id;
        let last_id=job.attrs.data.last_id;
        let setting=job.attrs.data.setting;//get setting ID
        let status=job.attrs.data.status;
        let nextRow=job.attrs.data.nextRow;
        let additionalData=job.attrs.data.additional;
        console.log("SCHEDULER RUN AT BROADCAST "+additionalData._id)


        //to set broadcast status if all contact already finish Processing, Failed and others
        let broadcastStatus="";
        let nextRun="";

        let parameter={};
        parameter['contact_group']=additionalData.phonebook;
        if(nextRow!=0){
            parameter['_id']={'$gt':ObjectId(nextRow)};
        }
        try{
            if(status!="Paused"){

                //update call status to processing 
                if(nextRow==0){
                    broadcastStatus="Processing";
                    await broadcast.updateCallStatus(additionalData._id,broadcastStatus);
                }
                //get setting 
                let callSetting=await vSetting.getCallSetting(setting._id);
    
                if(callSetting){
                    let sumContactSubscribers=await subcriber.countSubscribers(additionalData._id,'in_queue');
                   // console.log(sumContactSubscribers);
                    if(sumContactSubscribers==0){
                        //console.log("hellowwww");
                        let contacts=await contactList.getContactList(parameter,parseInt(process.env.DEFAULT_CONCURRENT_LIMIT_PER_USERS));
                       // console.log("hello wordl");
                        if(contacts.length>0){
                            console.log("CONTAC"+contacts.length);
                            let last_id=0;
                            let i=1;
                            let subscriberCollection=[];
                            contacts.map((vd)=>{
                                let newSubscr={
                                    phone:vd.phone,
                                    created_at:moment().tz(SERVER_DEFAULT_TIMEZONE).format(),
                                    updated_at:moment().tz(SERVER_DEFAULT_TIMEZONE).format(),
                                    uuid:additionalData.uid,
                                    broadcast:additionalData._id,
                                    phone_id:vd._id,
                                    //call_setting:setting._id,
                                    status:"in_queue", //(in_queue, in_call, no_answer, answer,re_call, error, fail, finish)
                                    tryCall:1
                                }   
                                subscriberCollection.push(newSubscr);
                            });

                            let insertSubscriber=await subcriber.insertBulk(subscriberCollection);

                            Promise.all(insertSubscriber.map(async (val)=>{
                                await queue({_id:val._id,broadcast:additionalData._id,phone:val.phone,phone_id:val.phone_id});
                            }));

                            
                            job.attrs.data.nextRow=contacts[contacts.length-1]._id;
                            nextRun=scheduleSetting.settingNextrun(setting);
                        }else{
                            //finish
                            job.attrs.data.status="Finish";
                            broadcastStatus="Finish";
                            //setting next run according to settin masters
                            await broadcast.updateCallStatus(additionalData._id,broadcastStatus);
                            nextRun=null;
                        }
                    }else{
                        nextRun=moment().add(30,'seconds').tz(setting.timezone).format();   
                    }
                }else{
                    //if there is no setting then dont proccessed broadcast scheduling
                    broadcastStatus="Failed";
                    await broadcast.updateCallStatus(additionalData._id,broadcastStatus);
                    nextRun=null;
                }

            }else{
                broadcastStatus="Paused";
                await broadcast.updateCallStatus(additionalData._id,broadcastStatus);
                nextRun=null
            }
            
        }catch(err){
            console.log(err);
            nextRun=moment().add(30,'seconds').tz(setting.timezone).format();
        }
        job.attrs.nextRunAt=nextRun;
        await job.save();
        return done();
    })
}