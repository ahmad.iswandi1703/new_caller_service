
/**
 * 
 * @param {Object} ObjectCall 
 * {
 *  origination_caller_id_number
 *  phone_number
 *  accountcode
 *  contact_group
 *  app_id
 *  language
 *  talent
 *  contact_var
 * }
 */

const command=require('./Command');
const _=require('lodash');
// $channel_variables = $dialplan_variables."ignore_early_media=true";
// $channel_variables .= ",origination_number=$phone_1";
// // $channel_variables .= ",originate_timeout=".$setting->dialing_timeout;
// $channel_variables .= ",origination_caller_id_name='$broadcast_caller_id_name'";
// $channel_variables .= ",origination_caller_id_number=$broadcast_caller_id_number";
// $channel_variables .= ",domain_uuid=".$domainUuid;
// $channel_variables .= ",domain=".$domainName;
// $channel_variables .= ",domain_name=".$domainName;
// $channel_variables .= ",presence_data='$broadcast_accountcode'";
// $channel_variables .= ",accountcode='$broadcast_accountcode'";

let settings={
    //inherit_codec:true,
    //ignore_display_updates:true,
   // hangup_after_bridge:true,
    //continue_on_fail:true,
    ignore_early_media:true,
    hangup_after_bridge:true,
    fail_on_single_reject:"^^:UNALLOCATED_NUMNER:CALL_REJECTED:NORMAL_CLEARING:USER_BUSY",
}
function Call(ObjectCall){
    this.entrance_pbx=ObjectCall.entrance_pbx;
    console.log(this.entrance_pbx);
    this.domain_name=ObjectCall.domain_name;
    this.settings=settings;
    this.domain_uuid=ObjectCall.domain_uuid;
    this.broadcast_id=ObjectCall.broadcast_id;
    this.uuid=ObjectCall.uuid;
    this.origination_caller_id_number=ObjectCall.origination_caller_id_number;
    this.phone_number=ObjectCall.phone_number;
    this.accountcode=ObjectCall.accountcode;
    this.contact_var=ObjectCall.contact_var;
    this.contact_group=ObjectCall.contact_group;
    this.language=ObjectCall.language;
    this.talent=ObjectCall.talent;
    this.callback_url=ObjectCall.callback_url==undefined?"default":ObjectCall.callback_url;
}    


Call.prototype.makeCall=async function(conn,gateway_uuid){
    //it will return;
    //get redis call by gateway uuuid if exist then incr if not then make 
    return new Promise(async (resolve,reject)=>{
        setTimeout(()=>{
            reject("timeout ESL SERVER CALLER")
        },6000);
        try{
            //make call
            //generate setings
            let variableChannel=await this.generateSettings();
            // let gateway=await gatewayObj.getGateway();
            if(gateway_uuid!==''){
                let sofia=`bgapi sched_api +3 ${this.uuid} bgapi originate ${variableChannel}sofia/gateway/${gateway_uuid}/${this.phone_number} ${this.entrance_pbx} XML ${process.env.PBX_MAIN_DOMAIN}`;
                console.log(sofia);
                let commain=await command.api(conn, sofia);
                //send default callback to url and user callback_url
                resolve(true);
            }else{
                resolve(false);
            }
        }catch(err){
            console.log(err);
            reject();
        }
    })

}


//this will generated all variables for needed of variable
Call.prototype.generateSettings=async function(){
    //ignore_early_media=true,
    //origination_number=081381475159,
    //origination_caller_id_name='anonymous',
    //origination_caller_id_number=0000000000,
    //domain_uuid=b3cad8c0-5a4d-4c6f-bc17-84bdfc474389,
    //domain=pbx.ocatelkom.co.id,
    //domain_name=pbx.ocatelkom.co.id,
    //accountcode='pbx.ocatelkom.co.id'} 
    //this.settings["uuid"]=this.uuid;


    this.settings["origination_caller_id_number"]="'"+this.origination_caller_id_number+"'";
    this.settings["origination_number"]="'"+this.phone_number+"'";
    this.settings["domain_uuid"]="'"+this.domain_uuid+"'";
   // this.settings["phone_number"]=this.phone_number;
    this.settings["accountcode"]="'"+this.accountcode+"'";
   // this.settings["broadcast_id"]=this.broadcast_id;
    this.settings["domain"]="'"+this.domain_name+"'";
    this.settings["domain_name"]="'"+this.domain_name+"'";
    this.settings["presence_data"]="'"+this.broadcast_id+"'";
    //this.settings["contact_var"]=JSON.stringify(this.contact_var);
    //this.settings["contact_group"]=this.contact_group;
    //this.settings["language"]=this.language;
    //this.settings["talent"]=this.talent;    
    
    let generatedSettings="{";

    _.forOwn(this.settings,(value,key)=>{
        // generatedSettings+='"'+key+'"'+"="+'"'+value+'",';
        //generatedSettings+="'"+key+"'"+"="+value+",";
        generatedSettings+=key+"="+value+",";
    })
    generatedSettings=generatedSettings.substr(0,generatedSettings.length-1);
    generatedSettings+="}";
    return generatedSettings;

}


module.exports=Call
