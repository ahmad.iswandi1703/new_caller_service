
const command=require('./Command');
let parseString=require('xml2js').parseString;

var events = require('events');
/**
 * 
 * @param {*} conn 
 * @param {*} clientRedis 
 * 
 * This Gateway purposed to see capacity of gateways sip trunk
 * It will determine which gateway will used to call PSTN or mobile phone
 * There are list of Gateway sip trunk with its capacity of concurrent call
 * 
 */
function Gateway(conn,clientRedis){
    this.variable_redis_name="CallGatewaySiptrunk";
    this.conn=conn;
    this.clientRedis=clientRedis

    this.gateways=[];
    this.setGatewayList();
    this.gatewayStatus=new events.EventEmitter();
}
let propertyWarn={
    'CONCURRENT_CALL_EXCEED_LIMIT':"CONCURRENT_CALL_EXCEED_LIMIT",
    'GATEWAY_IS_NOT_REGED':"GATEWAY_IS_NOT_REGED",
    'INTERNAL_SERVER_ERROR':'INTERNAL_SERVER_ERROR',
    'THERE_IS_NO_GATEWAY_ACTIVE':'THERE_IS_NO_GATEWAY_ACTIVE'
}
//check gateway avaibility
Gateway.prototype.checkGateway=async function(uuid){
    //check if gateway actives 
    return new Promise(async (resolve,reject)=>{
        try{
            let status=await command.api(this.conn,`sofia xmlstatus gateway ${uuid}`);
            await parseString(status.getBody(),  function(err,result){
                resolve(true);
                // if(result){
                //     if(result.gateway.state[0]=='REGED'){
                //         resolve(true);
                //     }else{
                //         resolve( false);
                //     }
                // }else{
                //     resolve(false);
                // }
            })
        }catch(err){
           // console.log(err);
            resolve( false);
        }
    })

}

Gateway.prototype.getConcurent=function(){
    return this.gateways[0].concurrent;
}

Gateway.prototype.setGatewayList=function(){
    let gatewayList=process.env.GATEWAY_LIST_UUID;
    let gatewayListConcurrent=process.env.GATEWAY_LIST_CONCURRENT

    let split=gatewayList.split(',');
    let splitCo=gatewayListConcurrent.split(',');
    if(split.length>0){
        let i=0;
        split.map((val)=>{
            if(splitCo.length==split.length){
                this.gateways.push({uuid:val,concurrent:splitCo[i]});
            }else{
                this.gateways.push({uuid:val,concurent:process.env.GATEWAY_DEFAULT_CONCURRENT});
            }
            i=i+1;
        })
        
    }
 

}

Gateway.prototype.checkActiveCalls=async function(){
    return new Promise(async (resolve,reject)=>{
        setTimeout(()=>{
            reject("timeout ESL SERVER")
        },6000);
        try{
            let status=await command.api(this.conn,`show channels as json`);
            let body=JSON.parse(status.body);
            if(body.hasOwnProperty('row_count')){
                resolve(body.row_count)
            }else{
                resolve(0);
            }
        }catch(err){
            console.log(err);
            //reject(err);
            resolve(0);
        }
    })
}

Gateway.prototype.getGateway=async function(){
    //return gateway actives uuid from freeswitch
    //check concurrent call for each gateway from each users

    let o=this;
    let check=async function(nextuuid=0,uuid=0){

        let re="";
        let newUUUID=uuid;
        if(newUUUID==0){
            newUUUID=o.gateways[0].uuid;
        }

        return o.gateways[0].uuid;

        // try{
        //     let gatewayReged=await o.checkGateway(newUUUID);
        //     if(gatewayReged){
        //         //check if this gateway is already full 
        //         //do it redis in here;
        //         let result=await o.checkActiveCalls();
        //         if(result){
        //             if(result>o.gateways[nextuuid].concurrent){
        //                 //if gateways full and gateway fail over to another gateway if gateway more than 1 
        //                 //this gateway are full
        //                 o.gatewayStatus.emit("gateway_status",o.gateways[nextuuid+1].uuid,propertyWarn.CONCURRENT_CALL_EXCEED_LIMIT);
        //                 re=await check(nextuuid+1,o.gateways[nextuuid+1].uuid);
        //             }else{
        //                 re=o.gateways[nextuuid].uuid;
        //                 return re;
        //             }
        //         }else{
        //             re=o.gateways[nextuuid].uuid;   
        //             return re;
        //         }
        //         //else
        //     }else{
        //         if(nextuuid+1>=o.gateways.length){
        //             o.gatewayStatus.emit("gateway_status",o.gateways[nextuuid+1].uuid,propertyWarn.THERE_IS_NO_GATEWAY_ACTIVE);
        //             return re;
        //         }

        //         re=await check(nextuuid+1,o.gateways[nextuuid+1].uuid);
        //         return re;
        //     }
        // }catch(err){
        //     console.log(err);
        //     o.gatewayStatus.emit("gateway_status",newUUUID,propertyWarn.INTERNAL_SERVER_ERROR);
        //     return re;
        // }
    }
  
    //check this out
    return new Promise(async(resolve,reject)=>{
        let bahagia=await check();
        resolve(bahagia);
    })
}




module.exports=Gateway;