var winston = require('winston'),

  path = require('path'),
  transports = [];
  let {format}=require('winston');
  require('winston-daily-rotate-file');
// transports.push(
//   new winston.transports.DailyRotateFile({
//     name: 'file',
//     datePattern: '.yyyy-MM-ddTHH',
//     filename: path.join(__dirname, 'logs', 'log_file.log')
//   })
// )

// var logger = new winston.Logger({ transports: transports })


// define the custom settings for each transport (file, console)
var options = {
    file: {
      level: 'info',
      name: 'file',
      datePattern: 'YYYY-MM-DD',
      filename: path.join(appRoot, 'logs', 'log_file.log'),
      handleExceptions: true,
      json: true,
      maxsize: 5242880, // 5MB
      maxFiles: 5,
      colorize: false,
      format:format.combine(
          format.printf(info=>`[${info.timestamp}] ${info.level}: ${info.message}`)
      )
    },
    console: {
      level: 'debug',
      handleExceptions: true,
      json: false,
      colorize: true,
    },
  };
  
  // instantiate a new Winston Logger with the settings defined above
  var logger = new winston.createLogger({
    format: format.combine(
        format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' })
    ),
    transports: [
      new winston.transports.DailyRotateFile(options.file),
      new winston.transports.Console(options.console)
    ],
    exitOnError: false, // do not exit on handled exceptions
  });
  
  // create a stream object with a 'write' function that will be used by `morgan`
  logger.stream = {
    write: function(message, encoding) {
      // use the 'info' log level so the output will be picked up by both transports (file and console)
      logger.info(message);
    },
  };
  
  
  module.exports = logger;