const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const moment=require('moment-timezone');
const timezone=require('../_configs/config.json').SERVER_DEFAULT_TIMEZONE;
const DateNow=moment().tz(timezone);
const ObjectId=mongoose.Types.ObjectId

let BroadcastSubscriberSchema= new Schema({
    phone:String,
    created_at:Date,
    updated_at:Date,
    uuid:String,
    broadcast:{
        type:Schema.Types.ObjectId,
        ref:'BroadcastCall',
        require:true
    },
    call_setting:String,
    status:String, //(in_queue, in_call, no_answer, answer,re_call, error, fail, finish)
    tryCall:Number,
    data:Map
});


//
let vSubscriber=mongoose.model('BroadcastSubscriber',BroadcastSubscriberSchema);
//get broadcast subscribers

//update subscribers
//object val
exports.update=async function(id,status){
    return new Promise(async (resolve,reject)=>{
        try{
            let subS=await vSubscriber.findById(id);
            subS.status=status;
            subS.updated_at=DateNow;
            await subS.save();

            resolve();
        }catch(err){
            reject(err);
        }
    })
}