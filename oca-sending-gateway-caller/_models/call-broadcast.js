const mongoose=require('mongoose');
const Schema=mongoose.Schema;



let BroadcastCall= new Schema({
    uid:String,
    presence_id:String,
    campaign_name:String,
    account_code:String,
    application:String,
    phonebook:{
        type:Schema.Types.ObjectId,
        ref:'BroadcastGroup',
        require:true
    },
    call_setting:{
        type:Schema.Types.ObjectId,
        ref:'CallSetting',
        require:true
    },
    contact_count:Number,
    call_immediately:String,
    call_schedule:Date,
    call_status:String,
    call_estimated_time:String,
    user_id:String,
    created_at:Date,
    updated_at:Date
    //add property to add advanced call_scheduled

});

let vCall=mongoose.model('BroadcastCall',BroadcastCall);

exports.getBroadcast=async function(broadcast){

    return new Promise((resolve,reject)=>{
        try{
            redisClient.get(`callBroadcastByID:${broadcast}`,async (err,result)=>{

                if(result){
                    const results=JSON.parse(result);
                    resolve(results);
                }else{         
                    let resultData=await vCall.findById(broadcast);
                    if(resultData){
                       redisClient.setex(`callBroadcastByID:${broadcast}`,parseInt(process.env.REDIS_EXPIRED_DATA),JSON.stringify(resultData));
                        resolve(resultData);
                    }else{
                        reject("cannot found the data");
                    }
                }
            });
        }catch(err){
            reject(err);
        }
    })
}

exports.callBroadcast=mongoose.model('BroadcastCall',BroadcastCall);