const mongoose=require('mongoose');
const Schema=mongoose.Schema;


let broadcastContactGroupSchema= new Schema({
    contact_group_name:String,
    contact_group_file:String,
    content_desc:[String],
    created_at:Date,
    updated_at:Date,
    user_id:String
});

mongoose.model('BroadcastGroup',broadcastContactGroupSchema);