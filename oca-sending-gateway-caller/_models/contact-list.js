const mongoose=require('mongoose');
const Schema=mongoose.Schema;

let BroadcastContactList= new Schema({

    phone:String,
    name:String,
    messageError:String,
    status:String, //not whatsapp number, or anything, lock
    enabled:Boolean,
    created_at:Date,
    updated_at:Date,
    contact_group:{
        type:Schema.Types.ObjectId,
        ref:'BroadcastGroup',
        require:true
    },
    content_data:Map,
    user_id:String
});

let vContacts=mongoose.model('BroadcastContactList',BroadcastContactList);

exports.getContactList=async function(parameter, limit){
    return new Promise((resolve,reject)=>{
        vContacts.find(parameter).sort({'_id':1}).limit(limit).then(function(res){
            resolve(res);
        }).catch(function(err){
            reject(err);
        })
    })
}

exports.getContactByID=async function(id){
    return new Promise((resolve,reject)=>{
        vContacts.findById(id).then(function(res){
            resolve(res);
        }).catch(function(err){
            reject(err);
        })
    })
}