const path = require('path');
const mongoose = require('mongoose');
const chalk = require('chalk');
var amqp = require('amqplib/callback_api');
const dotenv=require('dotenv');

const {Pool, Client} =require('pg'); 

//gateway 
const Gateway=require("./libs/Gateway");
const Call=require("./libs/Call");
dotenv.config({
    path: './_configs/.env.config'
});
global.appRoot = path.resolve(__dirname);

let redisQ=require('redis');
var clientRedis=redisQ.createClient(6379,process.env.REDIS_SERVER);

clientRedis.on('connect', function() {
    console.log('connected with redis server');
});

global.redisClient=clientRedis;
/**
 * LOGGER INITIATION
 */
let loggers = require('./_configs/logger');
global.logger = loggers;
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);
mongoose.connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    poolSize: 10
});
mongoose.Promise = global.Promise;
mongoose.connection.on('error', (err) => {
    console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));
    process.exit();
});
const {ERROR_CODE}=require('./_configs/ERROR');
var esl = require('modesl');
//models

let Broadcast=require('./_models/call-broadcast');
let Subscriber=require('./_models/call-subscriber');
let ContactGroup=require('./_models/contact-group');
let Contact=require('./_models/contact-list');

// let a=new esl.Connection(process.env.FS_HOST,process.env.FS_PORT,process.env.FS_PASSWORD);
// a.on("esl::connect",(val)=>{
//     console.log("val");
// })

// a.on("error",(val)=>{
//     console.log(val);
//     console.log("errorhe ere" );
// })

// a.on("esl::end",(val)=>{
//     console.log(val);
//     console.log("erende" );
// })


//var Gw;
// var ESLConnection = new esl.Connection(process.env.FS_HOST, process.env.FS_PORT, process.env.FS_PASSWORD, async function() {
//     console.log("CLIENT HAS BEEN CONNECTED TO FS SERVER");
//    Gw=new Gateway(ESLConnection,clientRedis);
//     Gw.gatewayStatus.on('gateway_status',(uuid,reason)=>{
//         console.log(reason);
//         console.log(uuid);
//         //set notification to slack or others channel
//     })


//     let gatewayUUID=await Gw.getGateway();
//     console.log(gatewayUUID);
//     //check gateway
//     let active=await Gw.checkGateway(gatewayUUID);
//     console.log(active);
//     // let activeCall=await Gw.checkActiveCalls();
//     // console.log(activeCall);
//     let settings={};
//     settings["uuid"]="2232323";
//     settings["origination_caller_id_number"]=00000;
//     settings["phone_number"]="081381475159";
//     settings["accountcode"]="2232323";
//     settings["broadcast_id"]="2232323";
//     settings["contact_var"]={"tagihan":"123231","nomor":"3434343"};
//     settings["contact_group"]="12323";
//     settings["language"]="id-ID";
//     settings["talent"]="id-ID-Wavenet-A";
//     //loggers.info("making call");
//     // let vCall=new Call(settings);
//     // try{
//     //     console.log("herer");
//     //     await vCall.makeCall(ESLConnection,a);
//     // }catch(err){
//     //     console.log(err);
//     // }


//     start();

// })


var amqpConn = null;
async function start() {
  amqp.connect(process.env.AMQP, function(err, conn) {
    if (err) {
      console.error("[AMQP]", err.message);
      return setTimeout(start, 1000);
    }
    conn.on("error", function(err) {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] conn error", err.message);
      }
    });
    conn.on("close", function() {
      console.error("[AMQP] reconnecting");
      return setTimeout(start, 1000);
    });

    console.log("[AMQP] connected");
    console.log(process.env.AMQP);
    amqpConn = conn;

    whenConnected();
  });
}
//get ivr menu uuid
async function getIvrMenuInitiate(uuid){

    return new Promise((resolve,reject)=>{
        try{
            clientRedis.get(`ivrMenuInitiate:${uuid}`,async (err,result)=>{
                if(result){
                    console.log("GET FROM REDIS HERE");
                    const results=result;
                    resolve(results);
                }else{   
                    
                    try{
                        const pool = new Pool();
                        const client=await pool.connect();
                        let ivrSurvey =await client.query(`select * from v_ivr_survey_pivot where uuid='${uuid}' order by created_at asc LIMIT 1`);
                        let ivr_menu_uuid;

                        if(ivrSurvey){
                            ivr_menu_uuid=ivrSurvey.rows[0].ivr_menu_uuid;
                        }
                        ///close pg connection to avoid memory high usage in postgre db
                        await client.release();
                        await pool.end();

                        if(ivr_menu_uuid){
                            console.log("mble");
                           clientRedis.setex(`ivrMenuInitiate:${uuid}`,3600, ivr_menu_uuid);
                            return  resolve(ivr_menu_uuid);
                        }else{
                            return reject("cannot found the data");
                        }
                    }catch(err){
                        console.log("error database");
                        return reject(err);
                    }
                }
            });
        }catch(err){
            reject(err);
        }
    })

}
function connected(){
    return new Promise((resolve,reject)=>{
        let ESLConnection=new esl.Connection(process.env.FS_HOST, process.env.FS_PORT, process.env.FS_PASSWORD);
        ESLConnection.on("esl::connect",async (val)=>{
            resolve(ESLConnection);
        })

        setTimeout(()=>{
            reject("Cannot connected to the server ESL");
        },6000);
    })
}
function whenConnected() {

    amqpConn.createChannel(function (error1, channel) {
        if (closeOnErr(error1)) return;

        let queue="queue_call_existing";
        channel.assertQueue(queue, {
            durable: true
        });
        channel.prefetch(1);

        console.log(' [*] Waiting for logs. To exit press CTRL+C');
        channel.consume(queue, async function (msg) {
            console.log(" [x] %s:'%s'", msg.fields.routingKey);
            //do the task jobs
            let data=JSON.parse(msg.content.toString());
            //{_id:newSubscr._id,broadcast:additionalData._id,phone:vd.phone,phone_id:vd._id}
            //get broadcast

            try{
                let ESLConnection=await connected();
                let Gw=new Gateway(ESLConnection,clientRedis);
                let broadcast=await Broadcast.getBroadcast(data.broadcast);
                let ivr_menu_uuid=await getIvrMenuInitiate(broadcast.application);
                console.log(ivr_menu_uuid,"im herhhere")
                if(broadcast){
                    //get contact information
                    let contact=await Contact.getContactByID(data.phone_id);
                    if(contact){
                        let contact_var=JSON.parse(JSON.stringify(contact.content_data));
                            //do processing to call data to server
                        let settings={};
                        settings["uuid"]=broadcast.uid;
                        settings["origination_caller_id_number"]=00000;
                        settings["entrance_pbx"]=ivr_menu_uuid;
                        settings["domain_name"]=process.env.PBX_MAIN_DOMAIN;
                        settings["phone_number"]=data.phone.replace("+62","0");
                        settings["domain_uuid"]="9d6a3b26-79d2-4d81-becd-910bf2cc75c7";
                        settings["accountcode"]=broadcast._id;
                        settings["broadcast_id"]=broadcast._id;
                        settings["contact_var"]=contact_var
                        settings["contact_group"]=contact.contact_group;
                        settings["language"]="id-ID";
                        settings["talent"]="id-ID-Wavenet-A";
                        let gatewayUUID=await Gw.getGateway();
                        //check gateway
                        //let active=await Gw.checkGateway(gatewayUUID);
                        //if(active){
                            let vCall=new Call(settings);
                            try{
                            //make call here
                                //checkactive calls take too long
        
                                let activeCall=await Gw.checkActiveCalls();
                                console.log(Gw.getConcurent(),activeCall);
                                if(activeCall<Gw.getConcurent()){
                                    await vCall.makeCall(ESLConnection,gatewayUUID);
                                    await Subscriber.update(data._id,"in_call");
                                    channel.ack(msg);
                                }else{
                                    console.log("reject");
                                    channel.reject(msg,true);
                                }

                            }catch(err){
                                console.log("error ku neng kene, requeue lagi");
                                console.log(err);
                                await Subscriber.update(data._id,"in_queue");
                                console.log("reject");
                                channel.reject(msg,true);
                                //channel.ack(msg);
                                //channel.ack(msg);
                            }

                        //}else{
                            // console.log("reject1");
                            //await Subscriber.update(data._id,"failed");
                            //channel.reject(msg,true);
                            //channel.ack(msg);
                            //channel.reject(msg,true);
                        //}
                    }else{
                        await Subscriber.update(data._id,"failed");
                        channel.ack(msg);
                    }
                    //check gateway error 
                    //send to caller server
                    //update status
                }else{
                    await Subscriber.update(data._id,"failed");
                    channel.ack(msg);
                }
            }catch(err){
                console.log("error neng kene 2");
                console.log(err);
                // await Subscriber.update(data._id,"failed");
                // //channel.ack(msg);
                // channel.ack(msg);
                await Subscriber.update(data._id,"in_queue");
                console.log("");
                channel.reject(msg,true);
            }
    
            
            // ESLConnection.on("error",async (val)=>{
            //     await Subscriber.update(data._id,"failed");
            //     channel.ack(msg);
            // })
            
            // ESLConnection.on("esl::end",async (val)=>{
            //     await Subscriber.update(data._id,"failed");
            //     channel.ack(msg);
            // })

            // setTimeout(()=>{
            //     channel.ack(msg);
            // },6000);

            //insert subscribers
        }, {
            noAck: false
        });
    });
}

function closeOnErr(err) {
    if (!err) return false;
    console.error("[AMQP] error", err);
    logger.info(`${ERROR_CODE.QUEUE_SERVER_ERROR} ${err}`);
    amqpConn.close();
    return true;
}


start();
// pbx01 = 165.22.48.43
// pbx02 = 159.65.5.252